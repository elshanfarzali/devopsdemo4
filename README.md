# DevOpsDemo4
## Deploying Spring PetClinic Sample Application on AWS EKS K8S using Terraform

# Note:
- **For accesing kubernetes dashboard:**

    * Get security token:
        ```
        kubectl -n kubernetes-dashboard get secret $(kubectl -n kubernetes-dashboard get sa/dashboard-admin -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"
        ```


    * Create tunnel:
        `kubectl proxy`


    * Access DASHBOARD:
        [K8 Dashboard](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/)
